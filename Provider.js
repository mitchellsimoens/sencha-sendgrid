const { Provider : Base } = require('@extjs/sencha-email');
const SendGrid            = require('sendgrid');
const { mail : Helper }   = SendGrid;

/**
 * @class Sencha.sendgrid.Provider
 * @extends Sencha.email.Provider
 *
 * A provider to interact with the Sendgrid API.
 */
class Provider extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} isSendgridProvider
                 */
                isSendgridProvider : true,

                /**
                 * @property {Object} typeMap A mapping of email
                 * body types.
                 * @private
                 */
                typeMap : {
                    html : 'text/html',
                    text : 'text/plain'
                },

                config : {
                    /**
                     * @cfg {String} key The Sendgrid API key.
                     */
                    /**
                     * @cfg {String} [method=POST]
                     */
                    method : 'POST',

                    /**
                     * @cfg {String} [path=/v3/mail/send]
                     */
                    path : '/v3/mail/send'
                }
            }
        };
    }

    ctor () {
        const { key } = this;

        if (key) {
            this.sendgrid = SendGrid(key);
        }
    }

    dtor () {
        this.sendgrid = null;
    }

    parseResBody (json, def) {
        if (json) {
            if (typeof json === 'string') {
                try {
                    json = JSON.parse(json);
                } catch(e) {
                    json = def;
                }
            }
        } else {
            json = def;
        }

        return json;
    }

    prepareBody (email = {}) {
        let { body } = email;

        if (body) {
            if (typeof body === 'string') {
                body = {
                    html : body
                };
            }

            const { typeMap } = this;

            for (let type in body) {
                const parsed = typeMap[type];

                if (parsed) {
                    body[parsed] = body[type];
                    delete body[type];
                }
            }
        }

        return body;
    }

    prepareMail (email) {
        const body = this.prepareBody(email);

        if (body) {
            const keys  = Object.keys(body);
            const first = keys.shift();
            const mail  = new Helper.Mail(
                new Helper.Email(email.from),
                email.subject,
                new Helper.Email(email.to),
                new Helper.Content(first, body[first])
            );

            keys.forEach(key => mail.addContent(new Helper.Content(key, body[key])));

            return mail;
        }
    }

    send (email) {
        return new Promise((resolve, reject) => {
            const { sendgrid } = this;

            if (sendgrid) {
                const mail = this.prepareMail(email);

                if (mail) {
                    const request = sendgrid.emptyRequest();

                    request.method = this.method;
                    request.path   = this.path;
                    request.body   = mail.toJSON();

                    sendgrid.API(
                        request,
                        (response) => {
                            let   body      = this.parseResBody(response.body, 'There was an error sending the email');
                            const hasErrors = body && Array.isArray(body.errors);

                            if (response.statusCode >= 400 || hasErrors) {
                                if (hasErrors) {
                                    body = body.errors.map(error => error.message).join('. ');
                                }

                                reject(new Error(body));
                            } else {
                                resolve({
                                    success : true
                                });
                            }
                        }
                    );
                } else {
                    reject(new Error('No body to send the email with'));
                }
            } else {
                reject(new Error('No sendgrid instance available'));
            }
        });
    }
}

module.exports = Provider;
