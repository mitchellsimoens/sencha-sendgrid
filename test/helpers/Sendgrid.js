module.exports = {
    createSendgrid ({ apiSpy, apiResult } = {}) {
        class Sendgrid {
            constructor (key) {
                this.key = key;
            }

            emptyRequest () {
                return {};
            }

            API (request, callback) {
                if (apiSpy) {
                    apiSpy(request, callback);
                }

                callback(apiResult);
            }
        }

        return key => {
            return new Sendgrid(key);
        };
    },

    createHelper () {
        const Content = this.createHelperContent();
        const Email   = this.createHelperEmail();
        const Mail    = this.createHelperMail();

        return {
            Content,
            Email,
            Mail
        };
    },

    createHelperContent () {
        class Content {
            constructor (type, content) {
                this.content = content;
                this.type    = type;
            }
        }

        return Content;
    },

    createHelperEmail () {
        class Email {
            constructor (address) {
                this.address = address;
            }
        }

        return Email;
    },

    createHelperMail () {
        class Mail {
            constructor (from, subject, to, content) {
                this.from    = from;
                this.subject = subject;
                this.to      = to;

                if (content) {
                    this.addContent(content);
                }
            }

            addContent (instance) {
                if (instance) {
                    let { content } = this;

                    if (content) {
                        content.push(instance);
                    } else {
                        this.content = [ instance ];
                    }
                }
            }
        }

        return Mail;
    }
};
