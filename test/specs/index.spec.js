const { expect } = require('chai');
const index      = require('../../');

describe('/index.js', function () {
    it('should retrieve Provider', function () {
        const test = index.Provider;

        expect(test).to.be.equal(require('../../Provider'));
    });
});
