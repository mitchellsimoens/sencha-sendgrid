const { expect }   = require('chai');
const { Provider } = require('../../');
const proxyquire   = require('proxyquire');

describe('Provider', function () {
    let instance;

    afterEach(function () {
        if (instance && !instance.destroyed) {
            instance.destroy();
        }

        instance = null;
    });

    describe('instantiation', function () {
        beforeEach(function () {
            instance = new Provider();
        });

        it('should be a provider', function () {
            expect(instance).to.have.property('isEmailProvider',    true);
            expect(instance).to.have.property('isSendgridProvider', true);
        });
    });

    describe('ctor', function () {
        it('should not create sendgrid', function () {
            instance = new Provider();

            expect(instance).to.not.have.property('sendgrid');
        });

        it('should create sendgrid', function () {
            instance = new Provider({
                key : 'abc'
            });

            expect(instance).to.have.property('sendgrid');
        });
    });

    describe('dtor', function () {
        it('should set sendgrid to null', function () {
            instance = new Provider({
                key : 'abc'
            });

            expect(instance).to.have.property('sendgrid');

            instance.destroy();

            expect(instance).to.have.property('sendgrid', null);
        });
    });

    describe('prepareBody', function () {
        beforeEach(function () {
            instance = new Provider();
        });

        it('should handle undefined', function () {
            const result = instance.parseResBody();

            expect(result).to.be.undefined;
        });

        it('should handle undefined by returning default', function () {
            const result = instance.parseResBody(undefined, 'This is a default');

            expect(result).to.equal('This is a default');
        });

        it('should parse an object', function () {
            const result = instance.parseResBody({
                foo : 'bar'
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('foo', 'bar');
        });

        it('should parse a string', function () {
            const result = instance.parseResBody(`{
                "foo" : "bar"
            }`);

            expect(result).to.be.an('object');
            expect(result).to.have.property('foo', 'bar');
        });

        it('should handle malformed json', function () {
            const result = instance.parseResBody(`{
                "foo" : "bar
            }`);

            expect(result).to.be.undefined;
        });

        it('should handle malformed json by returning default', function () {
            const result = instance.parseResBody(`{
                "foo" : "bar
            }`, 'This is a default');

            expect(result).to.equal('This is a default');
        });
    });

    describe('prepareBody', function () {
        beforeEach(function () {
            instance = new Provider();
        });

        it('should not return any body if undefined', function () {
            const result = instance.prepareBody();

            expect(result).to.be.undefined;
        });

        it('should prepare body as a string', function () {
            const result = instance.prepareBody({
                body : 'this is a body'
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('text/html', 'this is a body');
        });

        it('should preapre body with only html', function () {
            const result = instance.prepareBody({
                body : {
                    html : 'this is a body'
                }
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('text/html', 'this is a body');
            expect(Object.keys(result)).to.have.lengthOf(1);
        });

        it('should preapre body with only text', function () {
            const result = instance.prepareBody({
                body : {
                    text : 'this is a body'
                }
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('text/plain', 'this is a body');
            expect(Object.keys(result)).to.have.lengthOf(1);
        });

        it('should preapre body with both html and text', function () {
            const result = instance.prepareBody({
                body : {
                    html : 'this is a html body',
                    text : 'this is a text body'
                }
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('text/html',  'this is a html body');
            expect(result).to.have.property('text/plain', 'this is a text body');
            expect(Object.keys(result)).to.have.lengthOf(2);
        });

        it('should ignore unknown type', function () {
            const result = instance.prepareBody({
                body : {
                    foo  : 'bar',
                    html : 'this is a body'
                }
            });

            expect(result).to.be.an('object');
            expect(result).to.have.property('foo',       'bar');
            expect(result).to.have.property('text/html', 'this is a body');
            expect(Object.keys(result)).to.have.lengthOf(2);
        });
    });

    describe('prepareMail', function () {
        it('should not create mail', function () {
            instance = new Provider();

            const result = instance.prepareMail();

            expect(result).to.be.undefined;
        });

        it('should create mail', function () {
            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : {
                        mail : this.createHelper()
                    }
                }
            );

            instance = new Provider();

            const result = instance.prepareMail({
                body    : 'this is a body',
                from    : 'from@sencha.com',
                subject : 'this is a subject',
                to      : 'to@sencha.com'
            });

            expect(result).to.have.property('content');
            expect(result.content).to.be.an('array');
            expect(result.content).to.have.lengthOf(1);
            expect(result).to.have.deep.property('content[0].content', 'this is a body');
            expect(result).to.have.deep.property('content[0].type',    'text/html');

            expect(result).to.have.deep.property('from.address', 'from@sencha.com');

            expect(result).to.have.property('subject', 'this is a subject');

            expect(result).to.have.deep.property('to.address', 'to@sencha.com');
        });

        it('should add additional content', function () {
            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : {
                        mail : this.createHelper()
                    }
                }
            );

            instance = new Provider();

            const result = instance.prepareMail({
                body    : {
                    html : 'this is html body',
                    text : 'this is text body'
                },
                from    : 'from@sencha.com',
                subject : 'this is a subject',
                to      : 'to@sencha.com'
            });

            expect(result).to.have.property('content');
            expect(result.content).to.be.an('array');
            expect(result.content).to.have.lengthOf(2);
            expect(result).to.have.deep.property('content[0].content', 'this is html body');
            expect(result).to.have.deep.property('content[0].type',    'text/html');
            expect(result).to.have.deep.property('content[1].content', 'this is text body');
            expect(result).to.have.deep.property('content[1].type',    'text/plain');

            expect(result).to.have.deep.property('from.address', 'from@sencha.com');

            expect(result).to.have.property('subject', 'this is a subject');

            expect(result).to.have.deep.property('to.address', 'to@sencha.com');
        });
    });

    describe('send', function () {
        it('should not send if no key is used', function () {
            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : this.createSendgrid()
                }
            );

            instance = new Provider();

            return instance
                .send()
                .then(() => {
                    expect(false).to.be.true;
                })
                .catch(error => {
                    expect(error.message).to.equal('No sendgrid instance available');
                });
        });

        it('should not send if no email to send', function () {
            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : this.createSendgrid()
                }
            );

            instance = new Provider({
                key : 'abc'
            });

            return instance
                .send()
                .then(() => {
                    expect(false).to.be.true;
                })
                .catch(error => {
                    expect(error.message).to.equal('No body to send the email with');
                });
        });

        it('should send an email', function () {
            const apiSpy = this.sandbox.spy();

            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : this.createSendgrid({
                        apiSpy,
                        apiResult : {
                            body : {}
                        }
                    })
                }
            );

            instance = new Provider({
                key : 'abc'
            });

            return instance
                .send({
                    body    : 'this is a body',
                    from    : 'from@sencha.com',
                    subject : 'this is a subject',
                    to      : 'to@sencha.com'
                })
                .then(ret => {
                    expect(ret).to.be.an('object');
                    expect(ret).to.have.property('success', true);

                    expect(apiSpy).to.have.been.called;
                })
                .catch(() => {
                    expect(false).to.be.true;
                });
        });

        it('should handle a send error', function () {
            const apiSpy = this.sandbox.spy();

            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : this.createSendgrid({
                        apiSpy,
                        apiResult : {
                            body : {
                                errors: [
                                    {
                                        message : 'Error 1'
                                    },
                                    {
                                        message : 'Error 2'
                                    }
                                ]
                            }
                        }
                    })
                }
            );

            instance = new Provider({
                key : 'abc'
            });

            return instance
                .send({
                    body    : 'this is a body',
                    from    : 'from@sencha.com',
                    subject : 'this is a subject',
                    to      : 'to@sencha.com'
                })
                .then(() => {
                    expect(false).to.be.true;
                })
                .catch(error => {
                    expect(apiSpy).to.have.been.called;

                    expect(error.message).to.equal('Error 1. Error 2');
                });
        });

        it('should handle a send server error', function () {
            const apiSpy = this.sandbox.spy();

            const Provider = proxyquire(
                '../../Provider',
                {
                    sendgrid : this.createSendgrid({
                        apiSpy,
                        apiResult : {
                            statusCode : 500
                        }
                    })
                }
            );

            instance = new Provider({
                key : 'abc'
            });

            return instance
                .send({
                    body    : 'this is a body',
                    from    : 'from@sencha.com',
                    subject : 'this is a subject',
                    to      : 'to@sencha.com'
                })
                .then(() => {
                    expect(false).to.be.true;
                })
                .catch(error => {
                    expect(apiSpy).to.have.been.called;

                    expect(error.message).to.equal('There was an error sending the email');
                });
        });
    });
});
