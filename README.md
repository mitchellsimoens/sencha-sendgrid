# sencha-sendgrid

This module provides the [sencha-email](https://mitchell.io/sencha/sencha-email) module with a [Sendgrid](https://sendgrid.com/) provider.

This module does not require the `sencha-email` module, you must require it in your application along with this module.

## Add Provider

In order to create the `Provider` and add it to the `Manager` class, you must have the API key from Sendgrid.

    const { Manager }  = require('@extjs/sencha-email');
    const { Provider } = require('@extjs/sencha-sendgrid');

    Manager.add(
        'sendgrid',
        new Provider({
            key : '...'
        })
    );
